<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth;
use \App\Post;
use Illuminate\Pagination\Paginator;

use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    //
    public function __constuct()
    {
        $this->middleware('auth');
    }

    public function indexvue(){
        return view('posts.indexvue');
    }

    public function index()
    {
        if (auth()->user()) {
            $users = auth()->user()->following()->pluck('profiles.user_id');
            $userProfile = auth()->user()->profile;
            $posts = Post::whereIn('user_id', $users)->latest()->with('user')->paginate(5);
            // dd($icon);
            return view('posts.index', compact('posts', 'users', 'userProfile'));
        } else {
            return redirect('/login/');
        }
    }


    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        $data = request()->validate([
            //'antoher' => '' pass the data without validation
            'caption' => 'required',
            'image' => ['required', 'image']
        ]);

        $imagepath = request('image')->store('uploads', 'public');

        $image = Image::make(public_path("storage/{$imagepath}"))->fit(980, 980);
        $image->save();
        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image' => $imagepath,

        ]);

        // dd(request()->all());
        return redirect('/profile/' . auth()->user()->id);
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }
}
