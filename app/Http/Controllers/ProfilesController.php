<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{

    public function index(User $user)
    {
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;


        $postCount = Cache::remember(
            'count.posts.' . $user->id,
            now()->addSeconds(30),
            function () use ($user) {
                return $user->posts->count();
            }
        );
        $followersCount = $user->profile->followers->count();
        $followingCount = $user->following->count();


        return view('profile.index', compact('user', 'follows', 'postCount', 'followersCount', 'followingCount'));
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user->profile);

        return view('profile.edit', compact('user'));
    }
    public function update(User $user)
    {
        $this->authorize('update', $user->profile);
        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => 'url',
            'icon' => '',
        ]);



        if (request('icon')) {
            $imagePath = request('icon')->store('profile', 'public');

            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000, 1000);
            $image->save();
            // if the image is set
            $imageArr = ['icon' => $imagePath];
        }

        // dd(array_merge(
        //     $data,
        //     ['icon' => $imagePath]
        // ));



        auth()->user()->profile->update(array_merge(
            $data,
            $imageArr ?? []
            //if the image is not set there's no changes in array

        ));

        return redirect("/profile/{$user->id}");
    }
}
