<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $guarded = [];


    public function profileImage()
    {
        $imagePath =  ($this->icon) ?  $this->icon : 'profile/HTnDD1mjkJ4nUYTlEru5V3jl3gnp3YQfTZX4TSoS.png';
        return '/storage/' . $imagePath;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class);
    }
}
