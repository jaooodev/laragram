@extends('layouts.app')

@section('content')

<div class="inbox-wrapper ">
    <div class="container">
        <div class="inbox">
            <div class="inbox-sidebar">
                <div class="sidebar-header">
                    <div class="sd-st-hd">
                        <p>Direct</p>
                    </div>
                    <div>
                        <img src="/svg/write.svg" alt="">
                    </div>

                </div>
                <div class="chat-body">
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>
                    <div class="chat-user">
                        <div class="cu-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="cu-name">
                            <p>name</p>
                            <p>name</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="inbox-body">
                <div class="body-header">
                    <div class="st-bh">
                        <div class="bh-icon">
                            <img src="/storage/profile/9FyW8jGCXm8Uaw82HeMpNljvVKyZTNMKIYBLTDfr.jpeg" alt="">
                        </div>
                        <div class="bh-name">
                            <p>name</p>
                        </div>
                    </div>

                    <div class="nd-bh">
                        <img src="/svg/info.svg" alt="">
                    </div>

                </div>
                <div class="message-box">
                    <br>
                    <div class="sender">
                        <p>hello</p>

                    </div>
                    <br>

                    <div class="receiver">
                        <p>hi</p>
                    </div>


                </div>
                <div class="message-footer">
                    <img src="/svg/smile.svg" alt="" class="st-img">
                    <textarea placeholder="Message..." class="message-input"></textarea>
                    <img src="/svg/image.svg" alt="" class="nd-img">
                    <img src="/svg/heart.svg" alt="" class="rd-img">
                </div>
            </div>
        </div>
    </div>


</div>


@endsection
