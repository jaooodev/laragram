@extends('layouts.app')

@section('content')
<div class="container">

    <div class="container-feed">
        <div class="post">
            <div class="my-day-section">

            </div>
            @foreach($posts as $post)
            <div class="card">
                <div class="card-header">
                    <img src="/storage/{{$post->user->profile->icon}}" alt="">
                    <a href="/profile/{{$post->user->id}}" class=" pl-2">{{$post->user->username}}</a>

                </div>
                <div class="card-body p-0">

                    <a href="/profile/{{$post->user->id}}">
                        <img src="/storage/{{$post->image}}" alt="" class="w-100">
                    </a>
                    <div class="post-action">
                        <div class="st-block">


                            <img src="/svg/heart.svg" alt="">


                            <img src="/svg/chat.svg" alt="">


                            <img src="/svg/cursor-fill.svg" alt="">


                        </div>
                        <div class="nd-block">
                            <div class="bookmark">
                                <img src="/svg/bookmark.svg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="post-caption pl-3 pr-3">
                        <p>399 likes</p>
                        <p>{{$post->user->username}}</p>
                    </div>
                </div>

                <div class="card-footer" style="background-color:white;">
                    <!-- Material input -->
                    <div class="comment-section">
                        <textarea name="" id="" placeholder="Add a comment..."></textarea>
                        <p>POST</p>
                    </div>
                </div>
            </div>
            </br>
            @endforeach


        </div>


        <div class="left-side">
            <div class="left-side-profile">

                <img src="/storage/{{$userProfile->icon}}" alt="">


                <a href="/profile/{{$userProfile->user->id}}" class=" pl-2">{{$userProfile->user->username}}</a>
            </div>

            <div class="suggestions">

            </div>
        </div>

    </div>




    <!-- <div class="row pt-2 pb-4">
    <div class="col-6 offset-3">
        <p>
            <span class="font-weight-bold">
                <a href="/profile/{{$post->user->id}}">
                    <span class="text-dark">{{$post->user->username}}</span>
                </a>
            </span> {{$post->caption}}

        </p>

    </div>

</div> -->

    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            {{$posts->links()}}
        </div>
    </div>
</div>

</div>
@endsection
