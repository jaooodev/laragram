@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/p/create" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="row">
            <div class="col-8">
                <img src="/storage/{{$post->image}}" class="w-100" alt="">
            </div>
            <div class="col-4">
                <div class="d-flex align-items-center">
                    <div class="pr-3">
                        <img src="/storage/{{$post->user->profile->icon}}" alt="" class="rounded-circle w-100" style="max-width:50px;">
                    </div>

                    <div class="font-weight-bold">
                        <a href="/profile/{{$post->user->id}}">
                            <span class="text-dark ">
                                <h5> {{$post->user->username}}</h5>
                            </span>
                        </a>

                    </div>
                    <div class="font-weight-bold pb-2">
                        <a href="#" class="pl-3">Follow</a>
                    </div>

                </div>
                <hr>
                <div>
                    <p> {{$post->caption}}</p>
                </div>
            </div>


        </div>

    </form>
</div>
@endsection
