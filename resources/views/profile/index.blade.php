@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="{{$user->profile->profileImage()}}" class="w-100" style="border-radius:50%;" alt="">
        </div>
        <div class="col-9 p-5">
            <div>
                <div class="d-flex justify-content-between align-items-baseline">
                    <div class="d-flex align-items-baseline pb-3">
                        <div class="pr-4 h4">{{$user->username}}</div>
                        @can ('view', $user->profile)
                        <follow-button user-id="{{$user->id}}" follows={{$follows}}></follow-button>
                        @endcan
                        @can ('update', $user->profile)
                        <div class="align-items-left"><a href="/profile/{{$user->id}}/edit" class="button">Edit Profile</a></div>
                        @endcan
                    </div>


                </div>

            </div>
            <div class="d-flex">
                <div class="float-center pr-5"><strong>{{$postCount}}</strong> Post</div>
                <div class="pr-5"><strong>{{$followersCount}}</strong> Followers</div>
                <div class="pr-5"><strong>{{$followingCount}}</strong> following</div>
            </div>
            <div class="pt-5 font-weight-bold">{{$user->profile->title}}</div>
            <div>{{ $user->profile->description }}</div>
            <div><a href="#">{{$user->profile->url}}</a></div>
        </div>

    </div>

    <div class="row justify-content-between align-items-baseline">

        <div class="col-3">
            <div class="align-items-center"><a href="/p/create">POST</a></div>
        </div>
        <div class="col-3">
            <a href="/p/create">Saved</a>
        </div>
        <div class="col-3">
            <a href="/p/create">Tagged</a>
        </div>

    </div>
    <div class="row pt-5">

        @foreach($user->posts as $post)
        <div class="col-4 pb-3 pr-0">
            <div>
                <a href="/p/{{$post->id}}">
                    <img src="/storage/{{ $post->image }}" class="w-100" alt="">
                </a>
            </div>
        </div>
        @endforeach

    </div>
</div>
@endsection
