<?php

use App\Events\WebsocketEvent;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Expr\FuncCall;




Auth::routes();

//axios
Route::post('follow/{user}', 'FollowController@store');
//end
//post
Route::get('/', 'PostsController@index')->name('home');
//end
//profiles
Route::get('/profile/{user}', 'ProfilesController@index')->name('profile.show');
Route::patch('/profile/{user}', 'ProfilesController@update')->name('profile.update');
Route::get('/profile/{user}/edit', 'ProfilesController@edit')->name('profile.edit');
//post
Route::get('/p/create', 'PostsController@create');
Route::get('/p/{post}', 'PostsController@show');
Route::post('/p', 'PostsController@store');

//testvue
Route::get('/testvue', 'PostsController@indexvue');

//message


//web socket channel

Route::get('/direct/inbox', function () {
    broadcast(new WebsocketEvent('datahehe'));
    return view('message.inbox');
});
